# UI Style Compoenents

This is a repository with mostly sass components. It will contain elements
for websites, to be easy to insert and easy to copy-paste them into another project.

It will include desktop views and also mobile views

## Tools used

+ scss - for css preprocessing
+ Node - recomended for an easy easy way to convert Sass code into css

## Structure

+ css - folder with the compiled css code
+ scss - the folder in which the actula code will be written
+ index.html - the html page in which will have the style elements

### Running the current config

Install node.
in a terminal, opened in the current directory write

```bash
npm install
```

After all of the elements are installed, you can write your scss code into the scss folder.
When you are done, in the terminal you write:

```bash
npm run scss
```

You can live the terminal open to watch for files to change. When it will detect a file change,
it will automatically update the css folder.